#!/usr/bin/env python3

import logging
import sdl2
import subprocess
import time

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)

sdl2.SDL_Init(sdl2.SDL_INIT_JOYSTICK)
joystick = sdl2.SDL_JoystickOpen(0)

last_volume = None

button_commands = {
    6: ["obs-cli", "switch-scene", "Video"],
    5: ["obs-cli", "switch-scene", "AFK"],
    7: ["obs-cli", "switch-scene", "ThumbUp"],
    4: ["obs-cli", "switch-scene", "RaiseHand"],
    8: ["/home/hjacobs/.local/bin/toggle-mic-mute.sh"],
}

last_button_state = {}

while True:
    sdl2.SDL_PumpEvents()

    try:
        for i in range(12):
            pressed = sdl2.SDL_JoystickGetButton(joystick, i)
            was_pressed = last_button_state.get(i)
            if pressed and not was_pressed:
                logger.info(f"Joystick button {i} was pressed")
                command = button_commands.get(i)
                if command:
                    subprocess.check_call(command)
            last_button_state[i] = pressed
        offset = 32768
        max_val = 65535
        value = max_val - (sdl2.SDL_JoystickGetAxis(joystick, 3) + offset)
        percentage = int((value / max_val) * 100)
        if last_volume is not None and last_volume != percentage:
            subprocess.check_call(["amixer", "-q", "set", "Master", f"{percentage}%"])
        last_volume = percentage
    except KeyboardInterrupt:
        raise
    except Exception as e:
        logger.exception(f"Failed to process joystick events: {e}")
    time.sleep(0.1)
